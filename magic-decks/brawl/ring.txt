Commander
1 Ashiok, Nightmare Muse (THB) 208

Deck
1 Gate to Seatower (HBG) 79
1 Takenuma, Abandoned Mire (NEO) 278
1 Sheoldred, the Apocalypse (DMU) 107
1 Sink into Stupor (MH3) 241
1 Sword of Forge and Frontier (ONE) 244
1 Gollum, Patient Plotter (LTR) 84
1 Commit /// Memory (AKR) 54
1 Scroll of Isildur (LTR) 69
1 Gollum's Bite (LTR) 85
9 Nazgûl (LTR) 100
1 Azure Beastbinder (BLB) 41
1 Go for the Throat (BRO) 102
1 Unearth (2X2) 96
1 Rona's Vortex (DMU) 63
1 Housemeld (Y25) 5
1 Soothing of Sméagol (LTR) 70
1 Sam's Desperate Rescue (LTR) 105
1 Horses of the Bruinen (LTR) 55
1 Temporal Manipulation (SPG) 0
1 Nadir Kraken (THB) 55
1 Memorial to Folly (DAR) 242
1 Rangers of Ithilien (LTR) 66
1 Wash Away (VOW) 87
1 Malakir Rebirth (ZNR) 111
1 Glorious Gale (LTR) 51
1 The Black Breath (LTR) 78
1 Sudden Setback (MKM) 72
1 Mana Drain (OTP) 11
1 Claim the Precious (LTR) 81
1 Mischievous Mystic (FDN) 47
1 Ringsight (LTR) 220
1 Starving Revenant (LCI) 123
1 Kitnap (BLB) 53
1 Fatal Push (KLR) 84
1 Call of the Ring (LTR) 79
1 Dreadful as the Storm (LTR) 48
1 Jace, the Mind Sculptor (BLC) 75
1 Birthday Escape (LTR) 43
1 Witch-king of Angmar (LTR) 114
1 Sea Gate Restoration (ZNR) 76
1 Vanish from Sight (DSK) 82
1 Teferi's Ageless Insight (M21) 76
1 Consign /// Oblivion (AKR) 230
1 Dig Through Time (KTK) 36
1 Memory Deluge (MID) 62
1 Enduring Curiosity (DSK) 51
1 Gixian Puppeteer (BRO) 99
1 Time Warp (STA) 22
1 Karn's Temporal Sundering (DAR) 55
1 Gate of the Black Dragon (HBG) 77
1 Glasspool Mimic (ZNR) 60
6 Island (HBG) 295
1 Mystic Sanctuary (ELD) 247
2 Snow-Covered Island (KHM) 279
1 Sauron's Ransom (LTR) 225
1 Deep Analysis (MH3) 268
1 Lórien Revealed (LTR) 60
1 Bojuka Bog (WWK) 132
1 Gandalf, Friend of the Shire (LTR) 50
1 Hall of Storm Giants (AFR) 257
1 Otawara, Soaring City (NEO) 271
1 Ringwraiths (LTR) 284
1 Emrakul's Messenger (MH3) 61
1 Arid Archway (OTJ) 252
1 Bonders' Enclave (IKO) 245
1 Soulstone Sanctuary (FDN) 133
1 Fell the Profane (MH3) 244
1 Choked Estuary (SIR) 264
1 Clearwater Pathway (ZNR) 260
1 Darkslick Shores (ONE) 250
1 Restless Reef (LCI) 282
1 Port of Karfell (KHM) 265
1 Gloomlake Verge (DSK) 260
1 Shipwreck Marsh (MID) 267
1 Underground River (BRO) 267
1 Watery Grave (GRN) 259
1 Command Tower (ELD) 333
1 Dust Bowl (OTP) 65
4 Swamp (HBG) 300
2 Snow-Covered Swamp (KHM) 281
1 Inherited Envelope (LTR) 242

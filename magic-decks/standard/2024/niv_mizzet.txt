Deck
1 Plains (SLD) 63
1 Forest (HBG) 308
1 Mountain (SLD) 66
4 Jetmir's Garden (SNC) 250
3 Lilah, Undefeated Slickshot (OTJ) 217
3 Lightning Helix (MKM) 218
1 Swamp (SLD) 65
1 Flotsam // Jetsam (MKM) 247
4 Timeless Lotus (DMU) 239
1 Stormcarved Coast (VOW) 265
1 Cease // Desist (MKM) 246
1 Shivan Reef (DMU) 255
4 Ill-Timed Explosion (MKM) 207
1 Wail of the Forgotten (LCI) 244
2 No More Lies (MKM) 221
1 Angelfire Ignition (MID) 209
4 Niv-Mizzet, Supreme (MAT) 40
1 Siphon Insight (MID) 241
1 Dire-Strain Rampage (MID) 219
2 Ziatora's Proving Ground (SNC) 261
1 Legions to Ashes (BRO) 215
1 Sundown Pass (VOW) 266
2 Molten Collapse (LCI) 234
1 Island (SLD) 64
1 Soul Search (MKM) 232
1 Sunken Citadel (LCI) 285
2 Make Your Own Luck (OTJ) 218
4 Raffine's Tower (SNC) 254
4 Xander's Lounge (SNC) 260
4 Spara's Headquarters (SNC) 257
1 Ancient Cornucopia (BIG) 16
1 Push // Pull (MKM) 250

Sideboard
1 Unlicensed Hearse (SNC) 246
1 Cease // Desist (MKM) 246
1 Lightning Helix (MKM) 218
2 No More Lies (MKM) 221
1 Soul Search (MKM) 232
1 Molten Collapse (LCI) 234
2 Cease // Desist (MKM) 246
2 Badlands Revival (OTJ) 194
2 Doppelgang (MKM) 198
1 Flotsam // Jetsam (MKM) 247
1 Endless Detour (SNC) 183

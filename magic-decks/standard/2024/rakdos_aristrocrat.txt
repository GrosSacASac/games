Deck
1 Cult Conscript (DMU) 88
5 Swamp (HBG) 300
1 Liliana of the Veil (DMU) 97
4 Braids, Arisen Nightmare (DMU) 84
1 Takenuma, Abandoned Mire (NEO) 278
2 Phoenix Chick (DMU) 140
4 Mountain (HBG) 304
3 Yotia Declares War (DMU) 153
1 Faerie Dreamthief (WOE) 89
1 Sheoldred (MOM) 125
2 Sulfurous Springs (DMU) 256
2 Hall of Tagsin (BRO) 263
2 Transmogrant's Crown (BRO) 125
1 Soul Transfer (NEO) 122
1 Virus Beetle (NEO) 128
1 Tramway Station (SNC) 258
1 Sokenzan, Crucible of Defiance (NEO) 276
1 Jadar, Ghoulcaller of Nephalia (MID) 108
4 Haunted Ridge (MID) 263
3 Sticky Fingers (SNC) 124
1 Gixian Puppeteer (BRO) 99
2 Ob Nixilis, the Adversary (SNC) 206
4 Bloodtithe Harvester (VOW) 232
1 Lich-Knights' Conquest (WOE) 96
1 Tenacious Underdog (SNC) 97
1 Evolved Sleeper (DMU) 93
4 Blackcleave Cliffs (ONE) 248
1 Decadent Dragon (WOE) 223
1 Anoint with Affliction (ONE) 81
1 Vraan, Executioner Thane (ONE) 114
1 Blot Out (MAT) 12
1 Lord Skitter, Sewer King (WOE) 97

Sideboard
1 Yavimaya Steelcrusher (DMU) 152
1 Night Clubber (SNC) 89
1 Raze the Effigy (MID) 156
1 Rending Flame (VOW) 175
1 Graf Reaver (VOW) 115
2 Unlicensed Hearse (SNC) 246
1 Gix, Yawgmoth Praetor (BRO) 95
1 Torch Breath (SNC) 127
1 Whack (SNC) 99
1 Sheoldred, the Apocalypse (DMU) 107
2 Obstinate Baloth (BRO) 187
1 Sheoldred's Edict (ONE) 108
1 Furnace Reins (MOM) 141

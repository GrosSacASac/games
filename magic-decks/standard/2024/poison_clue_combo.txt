Deck
1 Helping Hand (LCI) 17
10 Plains (SLD) 63
1 Eiganjo, Seat of the Empire (NEO) 268
1 Three Blind Mice (WOE) 35
1 Hostile Negotiations (BRO) 105
2 Black Sun's Twilight (ONE) 84
1 Hall of Tagsin (BRO) 263
1 White Sun's Twilight (ONE) 38
1 Unstable Glyphbridge (LCI) 41
1 Takenuma, Abandoned Mire (NEO) 278
1 Sunken Citadel (LCI) 285
7 Swamp (SLD) 65
3 Fateful Absence (MID) 18
3 Bartolomé del Presidio (LCI) 224
1 Clay-Fired Bricks (LCI) 6
3 Persuasive Interrogators (MKM) 98
1 Teysa, Opulent Oligarch (MKM) 234
4 Scene of the Crime (MKM) 267
3 Depopulate (SNC) 10
4 Novice Inspector (MKM) 29
4 Foul Play (MID) 101
3 Homicide Investigator (MKM) 86
1 Corrupted Conviction (MOM) 98
1 Mondrak, Glory Dominus (ONE) 23
1 The Eternal Wanderer (ONE) 11

Sideboard
2 Invasion of Gobakhan (MOM) 22
2 Dreadmaw's Ire (LCI) 147
2 Break the Spell (WOE) 5
1 Loran's Escape (BRO) 14
1 Bonehoard Dracosaur (LCI) 134
1 Torch the Tower (WOE) 153
1 Sacred Fire (MID) 239
1 Angelfire Ignition (MID) 209
1 Valorous Stance (VOW) 42
3 Loran of the Third Path (BRO) 12

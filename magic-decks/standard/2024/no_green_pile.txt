Deck
3 Archangel of Wrath (DMU) 3
1 Shattered Sanctum (VOW) 264
1 Deserted Beach (MID) 260
4 Raffine's Tower (SNC) 254
1 Raff, Weatherlight Stalwart (MUL) 121
1 Otawara, Soaring City (NEO) 271
2 Sunken Citadel (LCI) 285
1 Stormcarved Coast (VOW) 265
1 Aether Channeler (DMU) 42
2 Negate (DMU) 58
1 Narset, Enlightened Exile (MAT) 38
3 Restless Anchorage (LCI) 280
1 Deduce (MKM) 52
3 Shipwreck Marsh (MID) 267
1 Push // Pull (MKM) 250
1 Twining Twins (WOE) 240
1 Haunted Ridge (MID) 263
2 Malicious Eclipse (LCI) 111
1 Foul Play (MID) 101
1 Adarkar Wastes (DMU) 243
2 Blackcleave Cliffs (ONE) 248
2 Memory Deluge (MID) 62
3 Cut Down (DMU) 89
1 Lier, Disciple of the Drowned (MID) 59
2 Sheoldred (MOM) 125
3 Sol'kanar the Tainted (DMU) 219
3 Sheoldred's Edict (ONE) 108
1 Go for the Throat (BRO) 102
1 Third Path Iconoclast (BRO) 223
2 Gix's Command (BRO) 97
1 Restless Fortress (WOE) 259
1 Unyielding Gatekeeper (MKM) 35
4 Xander's Lounge (SNC) 260
2 Branch of Vitu-Ghazi (MKM) 258

Sideboard
1 Runic Shot (DMU) 30
2 Obstinate Baloth (BRO) 187
2 Malicious Eclipse (LCI) 111
1 Realmbreaker, the Invasion Tree (MOM) 263
1 Hoverstone Pilgrim (LCI) 254
1 Disenchant (BRO) 6
1 Lucky Offering (NEO) 27
1 Loran of the Third Path (BRO) 12
2 Decadent Dragon (WOE) 223
3 Outrageous Robbery (MKM) 97

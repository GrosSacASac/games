Deck
14 Island (SLD) 64
2 Slip Out the Back (SNC) 62
3 Flow of Knowledge (BRO) 49
1 Geistwave (MID) 56
3 Negate (DMU) 58
2 Bitter Triumph (LCI) 91
1 Dissipate (MID) 49
4 Raffine's Tower (SNC) 254
2 Undercity Sewers (MKM) 270
4 Xander's Lounge (SNC) 260
4 Living Conundrum (MKM) 63
2 Anoint with Affliction (ONE) 81
3 Sleep-Cursed Faerie (WOE) 66
3 Essence Scatter (DMU) 49
4 Invoke the Winds (NEO) 58
2 Go for the Throat (BRO) 102
1 Sword of Forge and Frontier (ONE) 244
4 Take Flight (BRO) 65
1 Swamp (SLD) 65

Sideboard
3 Unlicensed Hearse (SNC) 246
2 Dissipate (MID) 49
2 Realmbreaker, the Invasion Tree (MOM) 263
1 Malicious Eclipse (LCI) 111
1 Dissipate (MID) 49
1 Realmbreaker, the Invasion Tree (MOM) 263
2 Angel of Suffering (SNC) 67
3 Coerced to Kill (MKM) 192

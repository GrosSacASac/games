Deck
1 Boseiju, Who Endures (NEO) 266
1 Eiganjo, Seat of the Empire (NEO) 268
3 The Wandering Emperor (NEO) 42
4 Gala Greeters (SNC) 148
1 Otawara, Soaring City (NEO) 271
1 Wedding Announcement (VOW) 45
4 Plains (SLD) 63
2 Forest (SLD) 67
4 Virtue of Loyalty (WOE) 38
1 Overgrown Farmland (MID) 265
1 Yavimaya Coast (DMU) 261
1 King Darien XLVIII (DMU) 204
1 Dreamroot Cascade (VOW) 262
4 Tocasia's Welcome (BRO) 30
1 Brushland (BRO) 259
1 Razorverge Thicket (ONE) 257
1 Deserted Beach (MID) 260
1 Seachrome Coast (ONE) 258
1 Adarkar Wastes (DMU) 243
2 Mirrex (ONE) 254
4 Skrelv's Hive (ONE) 34
3 Restless Anchorage (LCI) 280
2 Spara's Headquarters (SNC) 257
4 Protect the Negotiators (DMU) 62
4 Luxurious Libation (SNC) 152
4 Flip the Switch (MID) 54
1 Endless Detour (SNC) 183
1 Hullbreaker Horror (VOW) 63
1 Restless Vinestalk (WOE) 261
1 Restless Prairie (LCI) 281

Sideboard
1 Malevolent Hermit (MID) 61
2 Unlicensed Hearse (SNC) 246
2 Destroy Evil (DMU) 17
1 Knockout Blow (SNC) 20
1 Loran of the Third Path (BRO) 12
1 Endless Detour (SNC) 183
2 Obstinate Baloth (BRO) 187
1 Knockout Blow (SNC) 20
2 Loran of the Third Path (BRO) 12
1 Tishana's Tidebinder (LCI) 81
1 Surge of Salvation (MOM) 41

Deck
2 Syncopate (VOW) 83
1 Otawara, Soaring City (NEO) 271
1 Stormcarved Coast (VOW) 265
4 Mountain (SLD) 66
4 Shivan Reef (DMU) 255
1 Sokenzan, Crucible of Defiance (NEO) 276
4 Fires of Victory (DMU) 123
1 Swiftwater Cliffs (MOM) 273
9 Island (SLD) 64
1 Restless Spire (WOE) 260
3 Melek, Reforged Researcher (MKM) 430
2 Case of the Ransacked Lab (MKM) 45
1 Impulse (DMU) 55
2 Devious Cover-Up (MID) 48
2 Witness the Future (VOW) 90
2 Thundering Falls (MKM) 269
2 Jailbreak Scheme (OTJ) 53
4 Three Steps Ahead (OTJ) 75
3 Plan the Heist (OTJ) 62
1 Explosive Derailment (OTJ) 122
2 Arid Archway (OTJ) 252
4 Ill-Timed Explosion (MKM) 207
2 Alchemist's Gambit (VOW) 140
2 Memory Deluge (MID) 62
1 Seize the Storm (MID) 158

Sideboard
1 Devious Cover-Up (MID) 48
2 Flotsam // Jetsam (MKM) 247
1 Change the Equation (MOM) 50
2 Defabricate (BRO) 45
1 Obliterating Bolt (BRO) 145
1 Realmbreaker, the Invasion Tree (MOM) 263
1 Brotherhood's End (BRO) 128
1 Defabricate (BRO) 45
1 Unlicensed Hearse (SNC) 246
4 Third Path Iconoclast (BRO) 223

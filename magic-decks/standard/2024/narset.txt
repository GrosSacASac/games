Deck
2 Island (BRO) 271
2 Fading Hope (MID) 51
1 Furnace Reins (MOM) 141
1 Loran's Escape (BRO) 14
1 Otawara, Soaring City (NEO) 271
1 Swiftwater Cliffs (M21) 251
2 Stormcarved Coast (VOW) 265
2 Sundown Pass (VOW) 266
2 Invasion of Segovia (MOM) 63
1 Plains (HBG) 290
1 Sokenzan, Crucible of Defiance (NEO) 276
1 Cut Short (MOM) 10
1 Valorous Stance (VOW) 42
2 Shivan Reef (DMU) 255
1 Crystal Grotto (WOE) 254
2 Soul Partition (BRO) 26
2 Protect the Negotiators (DMU) 62
2 Mountain (DMU) 273
1 Adarkar Wastes (DMU) 243
2 Deserted Beach (MID) 260
2 Seachrome Coast (ONE) 258
1 Raff, Weatherlight Stalwart (DMU) 212
1 Battlefield Forge (BRO) 257
4 Third Path Iconoclast (BRO) 223
2 Meeting of Minds (MOM) 66
4 Narset, Enlightened Exile (MAT) 38
1 Monastery Mentor (MOM) 28
2 Timely Interference (DMU) 70
1 Fortified Beachhead (BRO) 262
2 Wedding Announcement (VOW) 45
2 Consider (MID) 44
2 Bitter Reunion (BRO) 127
1 Twining Twins (WOE) 240
1 Frolicking Familiar (WOE) 226
1 Sacred Fire (MID) 239
1 Subterranean Schooner (LCI) 80
2 Skrelv, Defector Mite (ONE) 33

Sideboard
2 Sunder the Gateway (MOM) 39
2 Lithomantic Barrage (MOM) 152
1 Elspeth's Smite (MOM) 13
1 Surge of Salvation (MOM) 41
1 Torch the Tower (WOE) 153
2 Sunder the Gateway (MOM) 39
1 Urza's Rebuff (BRO) 71
1 Disdainful Stroke (KHM) 54
1 Blue Sun's Twilight (ONE) 43
2 Protect the Negotiators (DMU) 62
1 Loran of the Third Path (BRO) 12

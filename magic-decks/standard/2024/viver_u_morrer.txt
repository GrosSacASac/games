Deck
4 Plains (SLD) 63
5 Swamp (SLD) 65
1 Shattered Sanctum (VOW) 264
1 Scoured Barrens (MOM) 272
1 Takenuma, Abandoned Mire (NEO) 278
1 Eiganjo, Seat of the Empire (NEO) 268
4 Wedding Announcement (VOW) 45
2 Benalish Sleeper (DMU) 8
1 Cathar Commando (MID) 10
3 Braids, Arisen Nightmare (DMU) 84
2 Gravelighter (NEO) 98
2 Sunken Citadel (LCI) 285
2 Elas il-Kor, Sadistic Pilgrim (DMU) 198
3 Restless Fortress (WOE) 259
4 Rite of Oblivion (MID) 237
1 Jadar, Ghoulcaller of Nephalia (MID) 108
3 Liesa, Forgotten Archangel (MID) 232
2 Caves of Koilos (DMU) 244
2 Vat of Rebirth (ONE) 113
1 Drannith Ruins (MAT) 50
1 Case of the Stashed Skeleton (MKM) 80
2 Skrelv's Hive (ONE) 34
2 Mirrex (ONE) 254
1 Bitter Triumph (LCI) 91
1 Homicide Investigator (MKM) 86
2 Skullcap Snail (LCI) 119
2 Vraan, Executioner Thane (ONE) 114
1 Corpses of the Lost (LCI) 98
1 Lord Skitter, Sewer King (WOE) 97
1 Demolition Field (BRO) 260
1 Virtue of Persistence (WOE) 115

Sideboard
1 Lion Sash (NEO) 26
2 Knockout Blow (SNC) 20
2 Doorkeeper Thrull (MKM) 13
2 Invasion of Gobakhan (MOM) 22
1 Whack (SNC) 99
1 The Fall of Lord Konda (NEO) 12
1 The Cruelty of Gix (DMU) 87
1 Loran of the Third Path (BRO) 12
2 Outrageous Robbery (MKM) 97
2 Night Clubber (SNC) 89

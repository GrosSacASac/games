Deck
2 Cartographer's Survey (VOW) 190
7 Forest (SLD) 67
2 Swamp (SLD) 65
1 Dreamroot Cascade (VOW) 262
1 Boseiju, Who Endures (NEO) 266
1 Underground Mortuary (MKM) 271
1 The Irencrag (WOE) 248
1 Open the Way (MAT) 23
2 Shipwreck Marsh (MID) 267
1 Otawara, Soaring City (NEO) 271
1 Consider (MID) 44
4 Breach the Multiverse (MOM) 94
1 Colossal Skyturtle (NEO) 216
2 Hedge Maze (MKM) 262
1 Takenuma, Abandoned Mire (NEO) 278
2 Invasion of Zendikar (MOM) 194
4 Glimpse the Core (LCI) 186
2 Island (SLD) 64
4 Push // Pull (MKM) 250
1 Deathcap Glade (VOW) 261
3 Intrude on the Mind (MKM) 61
1 Sudden Setback (MKM) 72
1 Reenact the Crime (MKM) 70
4 Conspiracy Unraveler (MKM) 47
2 Herd Migration (DMU) 165
1 Titan of Industry (SNC) 159
1 One with the Multiverse (BRO) 59
2 Repository Skaab (VOW) 73
1 Raffine's Tower (SNC) 254
1 Xander's Lounge (SNC) 260
2 Analyze the Pollen (MKM) 150

Sideboard
1 Tranquil Frillback (MAT) 24
1 Whack (SNC) 99
1 Unlicensed Hearse (SNC) 246
2 Pugnacious Hammerskull (LCI) 208
1 Malicious Malfunction (NEO) 110
1 Sudden Setback (MKM) 72
2 Obstinate Baloth (BRO) 187
1 Whack (SNC) 99
1 Conduit of Worlds (ONE) 163
2 Hoverstone Pilgrim (LCI) 254
2 Mirrorshell Crab (NEO) 63

Deck
1 Barkform Harvester (BLB) 243
1 Fecund Greenshell (BLB) 171
1 Long River Lurker (BLB) 57
1 Oakhollow Village (BLB) 258
2 Dour Port-Mage (BLB) 47
1 Into the Flood Maw (BLB) 52
3 Island (BLB) 372
2 Polliwallop (BLB) 189
2 Clifftop Lookout (BLB) 168
4 Mistbreath Elder (BLB) 184
4 Sunshower Druid (BLB) 195
4 Pond Prophet (BLB) 229
3 Valley Mightcaller (BLB) 202
2 Fabled Passage (ELD) 244
7 Forest (M19) 278
1 Tranquil Frillback (MAT) 24
1 Lilysplash Mentor (BLB) 222
1 Lilypad Village (BLB) 255
1 Defiler of Vigor (DMU) 160
2 Dreamdew Entrancer (BLB) 211
3 Clement, the Worrywort (BLB) 209
2 Botanical Sanctum (OTJ) 267
1 Run Away Together (BLB) 67
2 Three Tree Scribe (BLB) 199
2 Restless Vinestalk (WOE) 261
2 Yavimaya Coast (DMU) 261
1 Thornwood Falls (ELD) 313
1 Virtue of Knowledge (WOE) 76
1 Negate (DMU) 58
1 Three Tree City (BLB) 260

Sideboard
1 Tranquil Frillback (MAT) 24
1 Silverback Elder (DMU) 177
3 Cease // Desist (MKM) 246
2 Negate (DMU) 58
2 Disdainful Stroke (WOE) 47
1 Wrenn and Realmbreaker (MOM) 217
1 Nissa, Ascended Animist (ONE) 175
2 Obstinate Baloth (BRO) 187
2 Hard-Hitting Question (MKM) 164

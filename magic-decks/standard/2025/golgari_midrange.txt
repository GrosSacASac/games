Deck
1 Gisa, the Hellraiser (OTJ) 89
1 Shoot the Sheriff (OTJ) 106
4 Swamp (SLD) 65
1 Sheoldred (MOM) 125
1 Sandstorm Verge (OTJ) 263
4 Cut Down (DMU) 89
2 Go for the Throat (BRO) 102
4 Glissa Sunslayer (ONE) 202
4 Forest (SLD) 67
4 Outrageous Robbery (MKM) 97
4 Restless Cottage (WOE) 258
1 Tear Asunder (DMU) 183
1 Kaervek, the Punisher (OTJ) 92
2 Llanowar Wastes (BRO) 264
1 Aclazotz, Deepest Betrayal (LCI) 88
3 Urgent Necropsy (MKM) 240
4 Freestrider Lookout (OTJ) 163
1 Duress (ONE) 92
1 Conduit of Worlds (ONE) 163
2 Virtue of Persistence (WOE) 115
1 Blossoming Tortoise (WOE) 163
2 Mosswood Dreadknight (WOE) 231
1 Cease // Desist (MKM) 246
4 Festering Gulch (OTJ) 257
2 Demolition Field (BRO) 260
4 Arid Archway (OTJ) 252

Sideboard
1 Obstinate Baloth (BRO) 187
1 Malicious Eclipse (LCI) 111
1 Duress (ONE) 92
2 Tear Asunder (DMU) 183
2 Conduit of Worlds (ONE) 163
3 Obstinate Baloth (BRO) 187
2 Glistening Deluge (MOM) 107
1 Tranquil Frillback (MAT) 24
2 Choking Miasma (DMU) 86

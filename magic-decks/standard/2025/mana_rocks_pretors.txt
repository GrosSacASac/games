Deck
25 Island (SLD) 64
2 Forest (SLD) 67
3 Essence Scatter (DMU) 49
1 The Temporal Anchor (BRO) 82
4 The Mightstone and Weakstone (BRO) 238
1 Mirrex (ONE) 254
2 Combat Courier (BRO) 77
4 Collector's Vault (WOE) 244
3 Sunken Citadel (LCI) 285
3 Negate (DMU) 58
2 Marauding Brinefang (LCI) 64
2 Swamp (SLD) 65
2 Undercity Sewers (MKM) 270
4 Demolition Field (BRO) 260
1 Rona, Herald of Invasion (MOM) 75
1 Candy Trail (WOE) 243
2 Hedge Maze (MKM) 262
4 Sunbird Standard (LCI) 262
1 Threefold Thunderhulk (LCI) 265
1 Pit of Offerings (LCI) 278
1 Elesh Norn, Mother of Machines (ONE) 10
1 Restless Fortress (WOE) 259
4 Omen Hawker (MOM) 70
1 Volatile Fault (LCI) 286
1 Three Bowls of Porridge (WOE) 253
4 The Enigma Jewel (LCI) 55
2 Urza, Lord Protector (BRO) 225
2 Surgical Skullbomb (ONE) 243
1 Elesh Norn (MOM) 12
2 Akal Pakal, First Among Equals (LCI) 44
2 Patchwork Banner (BLB) 247
1 Kitnap (BLB) 53
1 Gix, Yawgmoth Praetor (BRO) 95
1 Jin-Gitaxias (MOM) 65
1 Sheoldred, the Apocalypse (DMU) 107
1 Sheoldred (MOM) 125
4 Hall of Tagsin (BRO) 263
1 Vorinclex (MOM) 213
1 Braided Net (LCI) 47
4 Realmbreaker, the Invasion Tree (MOM) 263
1 Meteorite (DMU) 235
4 Fountainport (BLB) 253
1 Urza's Command (BRO) 70
2 Meticulous Archive (MKM) 264
4 Oaken Siren (LCI) 66
1 Urza, Powerstone Prodigy (BRO) 69
1 Thran Spider (BRO) 254
4 Restless Anchorage (LCI) 280
1 Timeless Lotus (DMU) 239
1 Scene of the Crime (MKM) 267
1 Shoreline Looter (BLB) 70
1 Season of Weaving (BLB) 68
4 Inverted Iceberg (LCI) 60
4 Portal to Phyrexia (BRO) 240
4 Forensic Gadgeteer (MKM) 57
4 Relic of Legends (DMU) 236
4 Magnifying Glass (MKM) 255
2 Buried Treasure (LCI) 246
1 Chrome Host Seedshark (MOM) 51
1 Chimil, the Inner Sun (LCI) 249
2 Plains (SLD) 63
1 Against All Odds (ONE) 1
4 One with the Multiverse (BRO) 59
1 The Irencrag (WOE) 248
3 Fabled Passage (BLB) 252

Sideboard
3 Obstinate Baloth (BRO) 187
1 Filter Out (MAT) 7
1 Cosmium Confluence (LCI) 181
1 Serum Snare (ONE) 68
1 Defabricate (BRO) 45
1 Hurkyl's Final Meditation (BRO) 52
1 Faerie Slumber Party (WOE) 311
1 Cryptic Coat (MKM) 50
1 Doppelgang (MKM) 198
2 Kitnap (BLB) 53

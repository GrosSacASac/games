Deck
4 Fleeting Distraction (FDN) 155
2 Swamp (FDN) 276
1 Kiora, the Rising Tide (FDN) 45
3 Fblthp, Lost on the Range (OTJ) 48
7 Island (FDN) 274
1 Eaten Alive (FDN) 172
1 Stab (FDN) 71
4 Darkslick Shores (ONE) 250
1 Etrata, Deadly Fugitive (MKM) 200
1 Homunculus Horde (FDN) 41
1 Gixian Puppeteer (BRO) 99
1 Living Conundrum (MKM) 63
4 Opt (ELD) 59
3 Sheoldred, the Apocalypse (DMU) 107
2 Proft's Eidetic Memory (MKM) 67
1 Dismal Backwater (M21) 245
1 Gloomlake Verge (DSK) 260
1 Restless Reef (LCI) 282
2 Temple of Deceit (THB) 245
2 Undercity Sewers (MKM) 270
4 Archmage of Runes (FDN) 30
4 Enter the Enigma (DSK) 52
4 Take the Fall (OTJ) 73
2 Timely Interference (DMU) 70
1 Case of the Ransacked Lab (MKM) 45
1 Ojer Pakpatiq, Deepest Epoch (LCI) 67
1 Duelist of the Mind (OTJ) 45

Sideboard
1 Lazav, Familiar Stranger (OTJ) 216
1 Kaito, Dancing Shadow (ONE) 204
1 Shore Up (DMU) 64
1 Deathmark (FDN) 601
1 Ruthless Negotiation (BLB) 108
1 Anoint with Affliction (ONE) 81
1 Ghost Vacuum (DSK) 248
1 Bitter Triumph (LCI) 91
1 Disfigure (BRO) 91
1 Kaito, Bane of Nightmares (DSK) 220
1 Cut Down (DMU) 89
1 Deathmark (FDN) 601
1 Fell (BLB) 95
1 Blot Out (MAT) 12
1 Unsummon (FDN) 599

Deck
2 Island (SLD) 64
7 Swamp (SLD) 65
4 Aftermath Analyst (MKM) 148
2 Titania, Voice of Gaea (BRO) 193
2 Undergrowth Recon (MKM) 181
2 Lumra, Bellow of the Woods (BLB) 183
3 Nissa, Resurgent Animist (MAT) 22
1 Iridescent Vinelasher (BLB) 99
7 Forest (SLD) 67
4 Fabled Passage (ELD) 244
1 Demolition Field (BRO) 260
1 Hidden Necropolis (LCI) 275
1 Pit of Offerings (LCI) 278
1 Negate (DMU) 58
2 Atraxa, Grand Unifier (ONE) 196
1 Plains (SLD) 63
4 Pitiless Carnage (OTJ) 98
3 Go for the Throat (BRO) 102
1 Blot Out (MAT) 12
1 Barkform Harvester (BLB) 243
1 Gix's Command (BRO) 97
1 Urborg Repossession (DMU) 114
2 Volatile Fault (LCI) 286
2 Gruesome Realization (BRO) 103
1 Cosmium Confluence (LCI) 181
1 Terror Tide (LCI) 127
2 Tear Asunder (DMU) 183
1 Doppelgang (MKM) 198
1 Argoth, Sanctum of Nature (BRO) 256
4 Spelunking (LCI) 213
1 Invasion of Arcavios (MOM) 61
1 Hidden Nursery (LCI) 276
2 Sunken Citadel (LCI) 285
2 Malicious Eclipse (LCI) 111
2 Cut Down (DMU) 89
4 Escape Tunnel (MKM) 261
1 Pillage the Bog (OTJ) 224
1 Evolving Wilds (BRO) 261
1 Terramorphic Expanse (ONE) 261
1 Jungle Hollow (MOM) 270
1 Virtue of Strength (WOE) 197

Sideboard
2 Malicious Eclipse (LCI) 111
2 Obstinate Baloth (BRO) 187
1 Pick Your Poison (MKM) 170
1 Doppelgang (MKM) 198
1 Tear Asunder (DMU) 183
1 Deadly Cover-Up (MKM) 83
3 Outrageous Robbery (MKM) 97
1 Obstinate Baloth (BRO) 187
1 Cosmium Confluence (LCI) 181
1 Tranquil Frillback (MAT) 24
1 Wear Down (BLB) 203

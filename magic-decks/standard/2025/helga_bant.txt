Deck
3 Overlord of the Floodpits (DSK) 68
4 Horned Loch-Whale (WOE) 53
2 Split Up (DSK) 32
1 Portal to Phyrexia (BRO) 240
1 Cathartic Parting (DSK) 171
2 Final Showdown (OTJ) 11
1 Tomb Trawler (OTJ) 250
1 Ice Out (WOE) 54
1 Overlord of the Mistmoors (DSK) 23
1 Beza, the Bounding Spring (BLB) 2
4 Up the Beanstalk (WOE) 195
1 Hivespine Wolverine (BLB) 177
1 White Sun's Twilight (ONE) 38
1 Elesh Norn, Mother of Machines (ONE) 10
4 Overlord of the Hauntwoods (DSK) 194
3 Helga, Skittish Seer (BLB) 217
1 Ghost Vacuum (DSK) 248
1 Dreamdew Entrancer (BLB) 211
4 Floodfarm Verge (DSK) 259
2 Edgewall Inn (WOE) 255
1 Vaultborn Tyrant (BIG) 20
1 Seachrome Coast (ONE) 258
2 Meticulous Archive (MKM) 264
2 Hushwood Verge (DSK) 261
2 Razorverge Thicket (ONE) 257
1 Brushland (BRO) 259
3 Lush Portico (MKM) 263
2 Botanical Sanctum (OTJ) 267
3 Hedge Maze (MKM) 262
1 Restless Vinestalk (WOE) 261
1 Lakeside Shack (DSK) 262
1 Thornwood Falls (ELD) 313
2 Yavimaya Coast (DMU) 261
2 Plains (HBG) 290
1 Island (HBG) 295
2 Forest (HBG) 306
1 Destroy Evil (DMU) 17
1 Day of Judgment (FDN) 140
1 Willowrush Verge (DFT) 270
3 Ride's End (DFT) 25

Sideboard
1 Take Up the Shield (DMU) 35
4 Obstinate Baloth (BRO) 187
1 Seraphic Steed (OTJ) 232
1 Into the Flood Maw (BLB) 52
2 Beluna's Gatekeeper (WOE) 43
1 Threadbind Clique (WOE) 239
1 Twining Twins (WOE) 240
4 Stormkeld Vanguard (WOE) 187

Deck
2 Plains (SLD) 63
2 Collector's Vault (WOE) 244
1 Tranquil Cove (MOM) 275
4 Island (SLD) 64
1 Elesh Norn, Mother of Machines (ONE) 10
1 Gilded Lotus (BRR) 17
4 Boom Box (OTJ) 241
2 Unstoppable Plan (DFT) 72
4 The Enigma Jewel (LCI) 55
4 Omen Hawker (MOM) 70
1 Spring-Loaded Sawblades (LCI) 36
1 Three Bowls of Porridge (WOE) 253
2 Ride's End (DFT) 25
1 Jin-Gitaxias (MOM) 65
1 Elesh Norn (MOM) 12
4 Realmbreaker, the Invasion Tree (MOM) 263
2 Mazemind Tome (M21) 232
1 Aether Syphon (DFT) 38
1 Hall of Tagsin (BRO) 263
1 Alabaster Host Intercessor (MOM) 3
4 Arid Archway (OTJ) 252
1 Timeless Lotus (DMU) 239
1 The Eternal Wanderer (ONE) 11
1 Unstable Glyphbridge (LCI) 41
1 Restless Anchorage (LCI) 280
2 Seachrome Coast (ONE) 258
2 Elspeth's Smite (FDN) 493
1 Esoteric Duplicator (BIG) 5
1 Meticulous Archive (MKM) 264
4 Floodfarm Verge (DSK) 259
2 Split Up (DSK) 32

Sideboard
2 Soul-Guide Lantern (WOE) 251
1 Negate (DMU) 58
1 Break the Spell (WOE) 5
1 Disdainful Stroke (WOE) 47
1 Annex Sentry (ONE) 2
1 Joust Through (FDN) 19
1 Obstinate Baloth (BRO) 187
1 Confiscate (FDN) 709
1 Malcator, Purity Overseer (ONE) 208
1 Phyrexian Censor (MOM) 31
1 Unstable Glyphbridge (LCI) 41
1 Transplant Theorist (ONE) 73
1 Flashfreeze (FDN) 590
1 Aegis Turtle (FDN) 150

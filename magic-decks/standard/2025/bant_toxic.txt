Deck
3 Crawling Chorus (ONE) 8
2 Skrelv, Defector Mite (ONE) 33
3 The Seedcore (ONE) 259
4 Mirrex (ONE) 254
1 Razorverge Thicket (ONE) 257
4 Skrelv's Hive (ONE) 34
4 Tyvar's Stand (ONE) 190
4 Sunken Citadel (LCI) 285
2 Overprotect (BLB) 185
3 Annex Sentry (ONE) 2
2 Slaughter Singer (ONE) 216
1 Phyrexian Censor (MOM) 31
2 Tyrranax Atrocity (ONE) 188
1 Restless Anchorage (LCI) 280
3 Plains (SLD) 63
4 Brushland (BRO) 259
3 Yavimaya Coast (DMU) 261
1 Virtue of Loyalty (WOE) 38
2 Ivy, Gleeful Spellthief (DMU) 201
4 Jawbone Duelist (ONE) 18
4 Venerated Rotpriest (ONE) 192
3 Aspirant's Ascent (ONE) 40

Sideboard
1 Aerial Boost (MOM) 2
2 Disenchant (BRO) 6
4 Soulless Jailer (ONE) 241
1 Soul-Guide Lantern (WOE) 251
2 Phyrexian Censor (MOM) 31
1 No Witnesses (MKM) 27
3 Elesh Norn (MOM) 12
1 Tocasia's Welcome (BRO) 30

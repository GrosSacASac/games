Deck
2 Shore Up (DMU) 64
6 Island (OTJ) 280
1 Yavimaya Coast (DMU) 261
4 Intrude on the Mind (MKM) 61
5 Forest (LTR) 270
1 Self-Reflection (LCI) 74
2 Essence Scatter (DMU) 49
4 Colossal Rattlewurm (OTJ) 159
4 Negate (DMU) 58
4 Three Steps Ahead (OTJ) 75
4 Lush Oasis (OTJ) 261
1 Fountainport (BLB) 253
1 Mirrex (ONE) 254
4 Galewind Moose (BLB) 173
1 Conduit Pylons (OTJ) 254
1 The Skullspore Nexus (LCI) 212
1 Botanical Sanctum (OTJ) 267
1 Portal to Phyrexia (BRO) 240
2 Scatter Ray (BRO) 61
2 Restless Vinestalk (WOE) 261
2 Into the Flood Maw (BLB) 52
4 Arid Archway (OTJ) 252
2 Pawpatch Formation (BLB) 186
1 Phantom Interference (OTJ) 61

Sideboard
4 Obstinate Baloth (BRO) 187
2 Minor Misstep (ONE) 64
1 Tranquil Frillback (MAT) 24
1 Tranquil Frillback (MAT) 24
1 Conduit of Worlds (ONE) 163
2 Sudden Setback (MKM) 72
2 Atraxa's Fall (MOM) 176
1 Doppelgang (MKM) 198
1 Dreamdew Entrancer (BLB) 211

Deck
4 Omen Hawker (MOM) 70
18 Island (SLD) 64
3 The Enigma Jewel (LCI) 55
4 Forensic Gadgeteer (MKM) 57
1 Fblthp, Lost on the Range (OTJ) 48
1 Surgical Skullbomb (ONE) 243
1 Goblin Firebomb (BRO) 235
1 Inverted Iceberg (LCI) 60
1 Esoteric Duplicator (BIG) 5
2 Transplant Theorist (ONE) 73
1 Timeless Lotus (DMU) 239
1 Candy Trail (WOE) 243
1 Soul-Guide Lantern (WOE) 251
4 Wizard's Rockets (LTR) 252
4 Boom Box (OTJ) 241
2 Collector's Vault (WOE) 244
1 Realmbreaker, the Invasion Tree (MOM) 263
1 The Mightstone and Weakstone (BRO) 238
2 Three Steps Ahead (OTJ) 75
1 Drafna, Founder of Lat-Nam (BRO) 47
1 Blue Sun's Twilight (ONE) 43
2 The Mycosynth Gardens (ONE) 256
1 Arid Archway (OTJ) 252
1 A-Hall of Tagsin (BRO) 263
1 Mirrex (ONE) 254
1 Sunken Citadel (LCI) 285

Deck
2 Temple of the Dragon Queen (AFR) 260
2 Forest (HBG) 308
3 Mountain (SLD) 66
4 Restless Cottage (WOE) 258
1 Order of Midnight (ELD) 99
2 Swamp (SLD) 65
3 Virtue of Persistence (WOE) 115
1 Reaper of Night (ELD) 102
4 Sunken Citadel (LCI) 285
4 Bonecrusher Giant (ELD) 115
4 Edgewall Inn (WOE) 255
1 Edgewall Innkeeper (ELD) 151
1 Chandra, Hope's Beacon (MOM) 134
4 Decadent Dragon (WOE) 223
1 Beanstalk Giant (ELD) 149
3 Stormkeld Vanguard (WOE) 187
4 Ziatora's Proving Ground (SNC) 261
4 Murderous Rider (ELD) 97
4 Mosswood Dreadknight (WOE) 231
4 Lucky Clover (ELD) 226
1 Tarnation Vista (BIG) 30
1 Flaxen Intruder (ELD) 155
1 Gumdrop Poisoner (WOE) 93
1 Virtue of Strength (WOE) 197
1 The World Tree (KHM) 275

Sideboard
3 Unlicensed Hearse (SNC) 246
1 Blot Out (MAT) 12
1 Into the Fire (MOM) 144
2 Embereth Shieldbreaker (ELD) 122
1 Barkform Harvester (BLB) 243
1 Realmbreaker, the Invasion Tree (MOM) 263
1 Reaper of Night (ELD) 102
3 Outrageous Robbery (MKM) 97
2 Lovestruck Beast (ELD) 165

Deck
3 Ruthless Negotiation (BLB) 108
3 Swamp (SLD) 65
1 Demogorgon's Clutches (AFR) 96
4 Bloodtithe Harvester (VOW) 232
2 Mountain (SLD) 66
1 Jagged Barrens (OTJ) 259
2 Blood Crypt (RNA) 245
3 Fatal Push (KLR) 84
2 Push // Pull (MKM) 250
4 Fable of the Mirror-Breaker (NEO) 141
1 Stalactite Stalker (LCI) 122
3 Hagra Mauling (ZNR) 106
2 Zoyowa Lava-Tongue (LCI) 245
2 Virtue of Persistence (WOE) 115
2 Back for Seconds (WOE) 80
2 Sheoldred, the Apocalypse (DMU) 107
2 Sheoldred (MOM) 125
2 Trumpeting Carnosaur (LCI) 171
1 Sokenzan, Crucible of Defiance (NEO) 276
1 Aclazotz, Deepest Betrayal (LCI) 88
1 Blackcleave Cliffs (ONE) 248
1 Blightstep Pathway (KHM) 252
4 Haunted Ridge (MID) 263
1 Malakir Rebirth (ZNR) 111
1 Laughing Jasper Flint (OTJ) 215
1 Ebondeath, Dracolich (AFR) 100
1 Nightmare Shepherd (THB) 108
1 Castle Locthwain (ELD) 241
1 Takenuma, Abandoned Mire (NEO) 278
1 Preacher of the Schism (LCI) 113
1 Restless Vents (LCI) 284
1 Fountainport (BLB) 253
1 Sunken Citadel (LCI) 285
2 Den of the Bugbear (AFR) 254

Sideboard
2 Krenko's Buzzcrusher (MKM) 136
1 Orcus, Prince of Undeath (AFR) 229
1 Thoughtseize (AKR) 127
1 Duress (ONE) 92
1 Thought Distortion (M20) 117
1 Bitter Triumph (LCI) 91
1 Brotherhood's End (BRO) 128
2 Blot Out (MAT) 12
2 Cling to Dust (THB) 87
1 Deep-Cavern Bat (LCI) 102
1 Decadent Dragon (WOE) 223
1 Fatal Push (KLR) 84

Deck
1 Striped Riverwinder (AKR) 82
1 Minor Misstep (ONE) 64
2 Jin-Gitaxias, Progress Tyrant (NEO) 59
4 Reenact the Crime (MKM) 70
1 Defabricate (BRO) 45
2 The Modern Age (NEO) 66
4 Malcolm, Alluring Scoundrel (LCI) 63
22 Island (SLD) 64
1 Three Steps Ahead (OTJ) 75
2 Rona, Herald of Invasion (MOM) 75
3 Agent of Treachery (M20) 43
1 Spell Pierce (NEO) 80
1 Negate (DMU) 58
2 Waker of Waves (M21) 84
1 Essence Scatter (DMU) 49
1 Miscast (M21) 57
1 Experimental Augury (ONE) 49
1 Threefold Thunderhulk (LCI) 265
2 Flow of Knowledge (BRO) 49
1 Serum Snare (ONE) 68
1 Nexus of Becoming (BIG) 25
3 Dollhouse of Horrors (VOW) 255
1 Ulamog, the Ceaseless Hunger (BFZ) 15
1 Phantom Interference (OTJ) 61
1 Arid Archway (OTJ) 252

Sideboard
1 Negate (DMU) 58
1 Defabricate (BRO) 45
1 Unlicensed Hearse (SNC) 246
3 Stoic Sphinx (OTJ) 71
1 Hornswoggle (RIX) 39
1 The Filigree Sylex (ONE) 227
4 Blue Sun's Twilight (ONE) 43
2 Hedron Archive (JMP) 468
1 Palladium Myr (M21) 234

Mono Blue Control

Deck
22x Island
Field of Ruin
Cryptic Command
Spell Burst
Cryptic Coat
Harbinger of the Seas
1x Chrome Host Seedshark
1x Deem Inferior
2x Boomerang
1x Threads of Disloyalty
2x The Bath Song
Shadow of the Second Sun
Jace, the Mind Sculptor
2x Ancestral Vision
2x Peek
1x Flow of Knowledge
Boom Box
2x Dismember
Fading Hope
1x Overlord of the Floodpits
1x Spell Pierce
Blue Sun's Twilight
2x Time Warp
3x Archmage's Charm
4x Counterspell
2x Stern Scolding
Minor Misstep
Lórien Revealed

Sideboard
Surgical Extraction
The Stone Brain
Ghost Vacuum
2x The Filigree Sylex
Batterskull
Threads of Disloyalty
Gut Shot
Winter Moon
Commence the Endgame
Propaganda
2x Mystical Dispute
2x Stern Scolding


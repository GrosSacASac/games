# Factorio mods

## Gameplay changing

 * https://mods.factorio.com/mod/EarlyExtensions (early game only)
 * https://mods.factorio.com/mods/raiguard/Krastorio2
 * https://mods.factorio.com/mod/IndustrialRevolution

## Small gameplay additions


 * https://mods.factorio.com/mod/bztitanium
 * https://mods.factorio.com/mod/bzlead
 * https://mods.factorio.com/mod/WaterAsAResource
 * https://mods.factorio.com/mod/UraniumRework
 * https://mods.factorio.com/mod/Reasonable_Wind_Turbine
 * https://mods.factorio.com/mod/modmashsplinterairpurifier
 * https://mods.factorio.com/mod/ZRecycling
 * https://mods.factorio.com/mod/BurnerOffshorePumpUpdate
 * https://mods.factorio.com/mod/CarStart
 * https://mods.factorio.com/mod/vehicle_physics_again
 * https://mods.factorio.com/mod/WoodDoesBurn
 * https://mods.factorio.com/mod/Repair_Turret

## More enemies

 * https://mods.factorio.com/mod/ArmouredBiters
 * Challenging https://mods.factorio.com/mod/Big-Monsters
 * Challenging https://mods.factorio.com/mod/Rampant

## More vehicules

 * https://mods.factorio.com/mods/Earendel/jetpack
 * https://mods.factorio.com/mod/Aircraft

 * https://mods.factorio.com/mod/aai-vehicles-laser-tank
 * https://mods.factorio.com/mod/aai-vehicles-flame-tumbler
 * https://mods.factorio.com/mod/aai-vehicles-flame-tank

## More belts

 * https://mods.factorio.com/mod/UltimateBelts

## Ergonomie

 * https://mods.factorio.com/mod/themightygugi_alwaysday
 * https://mods.factorio.com/mod/Shield-FX-Updated
 * https://mods.factorio.com/mod/long-reach-clone
 * https://mods.factorio.com/mod/Squeak%20Through
 * https://mods.factorio.com/mod/water-poles


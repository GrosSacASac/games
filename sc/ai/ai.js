import { createAgent, createEngine, createPlayer } from '@node-sc2/core/sc2.js';

import { Difficulty, Race } from '@node-sc2/core/constants/enums.js';

// import { pylonSystem } from "./system/pylons.js";
import {yo} from "./builds/yo.js";
// import {buildgas} from "./builds/build-gas.js";




// const MAP = `DiscoBloodbathLE`;
// const MAP = `Simple128`;
// const MAP = `WorldofSleepersLE`;
const MAP = `AcropolisLE`;


const bot = createAgent(/*workerRush*/
        // the second parameter of unit-based event consumers is the unit
    yo,
    );
// bot.use(buildgas);
// bot.use(defendColossus);
// bot.use(pylonSystem); // todo does not work

const engine = createEngine();

engine.connect().then(() => {
    return engine.runGame(MAP, [
        createPlayer({ race: Race.PROTOSS }, bot),
        createPlayer({ race: Race.RANDOM, difficulty: Difficulty.VERYHARD }),
    ]).catch(console.error);
});
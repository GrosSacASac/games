# ai

## setup

Download maps from https://github.com/Blizzard/s2client-proto#downloads into C:\Program Files (x86)\StarCraft II\Maps

npm ci

npm start

## story

pylon system does not work, and fixed build order is not very smart, going back to the primitives

can afford is not reliable when buying two different things in the same frame

## todo

- upgrade happais
- better builduing placement (stuck units)
- actually become smart
- stop long distance mining when nexus dead
- calculate value of army
- maybe retreat if too good in comparison
- kill remaining buildings
- attack with worker if necessary (now: attack with no active nexuses)
- balance worker mining accross bases
- saturate gas as priority (not a problem as long as we don't lose probes)
- check if building is infested before making units
- AI errors when trying to build second assimilator when I have expanded to a base with 1 rich vespene
    - check for geysers closer than 8 to your base position to know if there is 1 or 2 gas
    
- contribute back to original repo with suggestions and problems encountered
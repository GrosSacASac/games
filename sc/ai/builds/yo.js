export {yo, chainIfFalsyAndCatchAsync};
import {firstTruethy} from "fp-sac";
import * as builds from "./builds.js"
import { createAgent, createEngine, createPlayer, taskFunctions, createSystem } from '@node-sc2/core/sc2.js';
const { build, upgrade } = taskFunctions;

import {  distance, areEqual, avgPoints, closestPoint } from '@node-sc2/core/utils/geometry/point.js';
import { Alliance } from '@node-sc2/core/constants/enums.js';
import unitTypes from '@node-sc2/core/constants/unit-type.js';
import { SupplyUnitRace  } from '@node-sc2/core/constants/race-map.js';


const { 
    IMMORTAL,
    COLOSSUS,
    OBSERVER,
    ASSIMILATOR,
    GATEWAY,
    ZEALOT,
    STALKER,
    NEXUS,
    CYBERNETICSCORE,
    ROBOTICSFACILITY,
    ROBOTICSBAY,
    PYLON,
    PROBE,
} = unitTypes;

const chainIfFalsyAndCatchAsync = function(afunction, ...rest) {
    if (afunction) {
        const result = afunction()
        if (result) {
            // console.log(afunction.toString());
            // console.log(`returned truthy`);
            if (result.then) {
                result.catch(console.error)
            }
            return;
        }
        chainIfFalsyAndCatchAsync(...rest);
    }
};


const unitNames = Object.fromEntries(Object.entries({IMMORTAL,
    COLOSSUS,
    OBSERVER,
    ASSIMILATOR,
    GATEWAY,
    ZEALOT,
    STALKER,
    NEXUS,
    CYBERNETICSCORE,
    ROBOTICSFACILITY,
    ROBOTICSBAY,
    PYLON,
    PROBE,
    }).map(([key, value]) => {
    return [value, key];
}));

const GAS_WORKER = `GAS_WORKER`;
const CONSTRUCTING = `CONSTRUCTING`;
const STOP_SPENDING = `STOP_SPENDING`;
const maxSupply = 200;
const MAX_WORKER_WANTED = 85;
const pylonSupply = 8;
const shouldBuildPylons = (foodUsed, foodCap) => {
    if (foodCap >= maxSupply) {
        return false;
    }
    const margin = 2 + foodUsed / 18;
    return foodUsed + margin >= foodCap;     
};
const SUPPLY = {
    [COLOSSUS]: 6,
    [IMMORTAL]: 4,
    [PROBE]: 1,
    [OBSERVER]: 1,
    [STALKER]: 2,
    [ZEALOT]: 2,
};
const simpleState = {
    mineralFieldsGone: 0,
    minUnits: 30,
    futureBuilduings: [],
    danger: undefined,
};
const buildingsWeWantOrdered = builds.greedyAF.slice();
let currentMinerals = 0;
let currentGas = 0;
const reliableCanAffordAndPay = function(data, unitTypeId) {
    const unitType = data.getUnitTypeData(unitTypeId);

    const result = (
        (currentMinerals  >= unitType.mineralCost) &&
        (!unitType.vespeneCost || currentGas >= unitType.vespeneCost)
    );

    if (result) {
        currentMinerals -= unitType.mineralCost;
        currentGas -= Number(unitType.vespeneCost);
    }

    return result;
};
const refresh = function({ minerals, vespene }, Id) {
    currentMinerals = minerals;
    currentGas = vespene;

};

const enoughSupplyForUnit = (foodUsed, foodCap, unitID) => {
    const unitSupply = SUPPLY[unitID];
    return foodUsed + unitSupply <= foodCap;     
};

const getNextPylonPosition = function (resources) {
    const {  map } = resources.get();

    const [main ] = map.getExpansions();

    if (!main) {
        return []; // todo
    }
    const mainMineralLine = main.areas.mineralLine;
    const geysers = main.cluster.vespeneGeysers;

    const mainBase = main.getBase();
    const locations = main.areas.areaFill.filter((point) => {
        return (
            // not too far
            (distance(point, mainBase.pos) <= 25) &&
            // far enough away to stay outta the mineral line
            (mainMineralLine.every(mlp => {return distance(mlp, point) > 2;})) &&
            // far enough away from gas line
            (geysers.every(gp => {return distance(gp.pos, point) > 3;}))
        );
    });

    return locations;
    
};
const getNextBuilduingPosition = function (resources, id) {
    const {  map } = resources.get();

    // map.getAvailableExpansions()
    const [main, ...others ] = map.getExpansions(Alliance.SELF);

    if (!main) {
        return []; // todo
    }
    const mainMineralLine = main.areas.mineralLine;
    const geysers = main.cluster.vespeneGeysers;

    const mainBase = main.getBase();
    if (id === NEXUS) {
        
        
        const notOccupiedWallStreet = others.filter(expansion => {
            return !expansion.getBase()
        })
        notOccupiedWallStreet.sort(function(a, b) {
            return a.pathFromMain.length - b.pathFromMain.length;
        });
        
        return notOccupiedWallStreet.map(({townhallPosition}) => {
            return townhallPosition;
        }).slice(0,1);
    }
    // if (id === ASSIMILATOR) {
    //     return main.cluster.vespeneGeysers.map(({pos}) => {
    //         return pos;
    //     });
    // }
    const locations = main.areas.areaFill.filter((point) => {
        return (
            // not too far
            (distance(point, mainBase.pos) <= 25) &&
            // far enough away to stay outta the mineral line
            (mainMineralLine.every(mlp => {return distance(mlp, point) > 2;})) &&
            // far enough away from gas line
            (geysers.every(gp => {return distance(gp.pos, point) > 3;}))
        );
    });

    return locations;
    
};
const haveEnoughPylons = function ({ resources, agent, data }) {
    const { foodUsed, foodCap } = agent;
    const { units, actions } = resources.get();
    const supplyUnitId = SupplyUnitRace[agent.race];
    const buildAbilityId = data.getUnitTypeData(supplyUnitId).abilityId;


    // current supplyCap includes pylons currently building and existing orders given to build them
    const futureSupplyCap = (
        foodCap +
        (units.inProgress(supplyUnitId).length * pylonSupply / 2.2) + 
        (units.withCurrentOrders(buildAbilityId).length * pylonSupply)
    );
    
    if (!shouldBuildPylons(foodUsed, futureSupplyCap)) {
        return;
    }
    if (!reliableCanAffordAndPay(data, supplyUnitId)) {
        return;
    }
    const positions = getNextPylonPosition(resources);
    
    return preparePylonPlacement({ resources, positions, agent });
};

const preparePylonPlacement = async ({resources, positions, agent}) => {
    
    const supplyUnitId = SupplyUnitRace[agent.race];
    const { units, actions } = resources.get();
    const foundPosition = await actions.canPlace(supplyUnitId, positions);

    if (foundPosition) {
        
        const allWorkers = units.getWorkers(true);
        const closestWorker = closestWorkerWithNoQueue(units, foundPosition);
        if (!closestWorker) {
            return;
        }
        return actions.build(supplyUnitId, foundPosition, closestWorker);
    }
};


const idleBackToWork = function ({ resources, agent, data }) {
    const { foodUsed, foodCap } = agent;
    const { units, actions, map } = resources.get();
    if (units.getStructures().filter(x => {
        return x.isCurrent() && x.isFinished();
    }).map(({unitType}) => {
        return unitType;
    }).includes(NEXUS)) {
        return firstTruethy(idleWorker => {
            if (!idleWorker.isCurrent()) {
                return;
            }
            idleWorker.labels.delete(CONSTRUCTING);
            // mine gas
            if (idleWorker.labels.has(GAS_WORKER)) {
                const assimilator = idleWorker.labels.get(GAS_WORKER);
                if (assimilator.isCurrent() /* todo also check if empty */) {
                    return actions.mine([idleWorker], assimilator);
                }  // gas dead
                idleWorker.labels.delete(GAS_WORKER);
                
            }
            // or minerals
            
            return actions.gather(idleWorker);;
        }, units.getIdleWorkers());
    }
    const idles = units.getIdleWorkers();
    if (idles?.length) {
        console.log(`attacking with ${idles?.length} probes`);
        return attack({combatUnits: idles, actions, map});
    }
    
};
let i = 0;
/* based on the builduings we have, and the builduings we want
build the next one we want */
const haveTechWeWant = function ({ resources, agent, data }) {
    const { foodUsed, foodCap } = agent;
    const { units, actions } = resources.get();
    const whatWeAreGoingToHave = simpleState.futureBuilduings; // hollogram before builduing
    const whatWeHave = units.getStructures().map(({unitType}) => {
        return unitType;
    });
    const haveAndGoingToHave = [...whatWeHave, ...whatWeAreGoingToHave];
    
    
    i += 1;
    // if (i % 250 === 0) {
        // console.log({currentMinerals, currentGas});
        // console.log(`what we have And Going To Have:`);
        // haveAndGoingToHave.forEach(id => {
        //     if (id !== PYLON) {

        //         console.log(unitNames[id]);
        //     }
        // });
    // }
    const nextThingToBuild = buildingsWeWantOrdered.find(id => {
        const index = haveAndGoingToHave.indexOf(id);
        if (index === -1) {
            return true;
        } 
            haveAndGoingToHave.splice(index, 1);
        
    });
    // if (i % 250 === 0) {
    //     console.log(`nextThingToBuild`, unitNames[nextThingToBuild]);
    // }
    if (nextThingToBuild === undefined) {
        return;
    }
    if (!agent.hasTechFor(nextThingToBuild)) {
        return;
    }
    if (!reliableCanAffordAndPay(data, nextThingToBuild)) {
        return;
    }
    if (nextThingToBuild === ASSIMILATOR) {
        simpleState.futureBuilduings.push(nextThingToBuild);
        ((async () => {
            try {
                await actions.buildGasMine();
            } catch (error) {
                console.warn(error);
            }
        })());
        
    }
    const positions = getNextBuilduingPosition(resources, nextThingToBuild);
    
    return prepareBuilduingPlacement({ resources, positions, nextThingToBuild });
};

const closestWorkerWithNoQueue = (units, position) => {
    const allWorkers = units.getWorkers(true);
    const [closestWorker] = units.getClosest(position, allWorkers, 100).filter(worker => {
        return !worker.labels.has(CONSTRUCTING);
    });
    return closestWorker;
};


const prepareBuilduingPlacement = async ({ resources, positions, nextThingToBuild  }) => {
    const { units, actions } = resources.get();
    const foundPosition = await actions.canPlace(nextThingToBuild, positions);
    
    if (foundPosition) {
        // const closestWorker = units.getClosest(foundPosition, units.getMineralWorkers(), 1);
        try {
            const allWorkers = units.getWorkers(true);
            const closestWorker = closestWorkerWithNoQueue(units, foundPosition);
            if (!closestWorker) {
                return;
            }
            closestWorker.labels.set(CONSTRUCTING, nextThingToBuild);
            // no need to queue since it will take an innocent worker            
            const res = await actions.build(nextThingToBuild, foundPosition, closestWorker);
            simpleState.futureBuilduings.push(nextThingToBuild);
            return res;
        } catch (error) {
            console.error(error);
        }
    }
};
const attack = ({combatUnits, actions, map}) => {
    // FOR AIUR
    // NO RETREAT POSSIBLE
    // treason is 

    // get our enemy's bases...
    const [enemyMain, enemyNat, b3, b4, b5, b6, b7, b8] = map.getExpansions(Alliance.ENEMY);
    
    // queue up our army units to attack both bases (in reverse, natural first)
    return Promise.all([
        enemyNat,
        enemyMain,
        b3, b4, b5, b6, b7, b8,
        simpleState.danger || map.getCombatRally(),
    ].map((expansion) => {
        if (expansion === undefined) {
            return Promise.resolve();
        }
        return actions.attackMove(combatUnits, expansion.townhallPosition, true,
        );
    }));
};

const removeFromFutureBuilduings = (type) => {
    const index = simpleState.futureBuilduings.indexOf(type);
    if (index !== -1) {
        simpleState.futureBuilduings.splice(index, 1);
        // console.log(`removing`, unitNames[type]);
    }
};
const onBuilduingFinished = ({ resources, agent }, newUnit) => {
    const type = newUnit.unitType;
    removeFromFutureBuilduings(type);
    if (newUnit.isGasMine()) {
        const { units, actions } = resources.get();

        // get the three closest probes to the assimilator
        const threeWorkers = units.getClosest(newUnit.pos, units.getMineralWorkers(), 3);
        // add the `gasWorker` label, this makes sure they aren't used in the future for building
        threeWorkers.forEach(worker => {
            return worker.labels.set(GAS_WORKER, newUnit);
        });
        // send them to mine at the `newUnit` (the assimilator)
        return actions.mine(threeWorkers, newUnit);
    }
};

const makeProbes = ({data, units, seconds, actions, foodUsed, foodCap}) => {
    if (units.getWorkers(true).length >= MAX_WORKER_WANTED) {
        // console.log(`Too many workers`);
        return;
    }
    return firstTruethy(base => {
        if (reliableCanAffordAndPay(data,  PROBE) && base.isFinished()) {
            if (base.orders.length <= 0 || seconds >= 36 && base.orders.length <= 1) {
                if (enoughSupplyForUnit(foodUsed, foodCap, PROBE)) {
                    return actions.train(PROBE, base);
                    return true;
                }
            }
        }
    }, units.getBases());
};

const makeUnits = ({data, units, seconds, agent, actions, foodUsed, foodCap}) => {
    const idleRobos = units.getById(ROBOTICSFACILITY, { noQueue: true, buildProgress: 1 });
    if (idleRobos.length > 0) {
        return firstTruethy(robo => {
            if (agent.hasTechFor(COLOSSUS)) {
                
                let unitToBuild  = COLOSSUS;
                const randomResult = Math.random(); 
                if (randomResult > 0.8 && units.getById(OBSERVER).length < 3) {
                    unitToBuild = OBSERVER;
                } else if (randomResult > 0.4 && reliableCanAffordAndPay(data, COLOSSUS)) {
                    unitToBuild = IMMORTAL;
                }
                if (robo.isCurrent() && robo.isFinished() && reliableCanAffordAndPay(data, unitToBuild)) {
                    if (enoughSupplyForUnit(foodUsed, foodCap, unitToBuild)) {
                        return actions.train(unitToBuild, robo);
                    }
                }
            } else if (robo.isCurrent() && robo.isFinished() && reliableCanAffordAndPay(data, IMMORTAL)) {
                let unitToBuild  = IMMORTAL;
                if (units.getById(OBSERVER).length < 1) {
                    unitToBuild = OBSERVER;
                }
                if (enoughSupplyForUnit(foodUsed, foodCap, unitToBuild)) {
                    return actions.train(unitToBuild, robo);
                }
            }
        }, idleRobos);
    }
    const idleGateways = units.getById(GATEWAY, { noQueue: true, buildProgress: 1 });
    if ((idleRobos.length === 0 || currentMinerals > 1000) && (idleGateways.length > 0)) {
        // if there are some, send a command to each to build a zealot
        return firstTruethy(gateway => {
            let unitToBuild  = ZEALOT;
            if (Math.random() > 0.4) {
                unitToBuild = STALKER;
            }
            if (gateway.isCurrent() && reliableCanAffordAndPay(data, unitToBuild)) {
                if (agent.hasTechFor(unitToBuild)) {
                    if (enoughSupplyForUnit(foodUsed, foodCap, unitToBuild)) {
                        return actions.train(unitToBuild, gateway);
                    }
                }
            }
        }, idleGateways);
    }
};



const nonWorkerCreated = ({actions, map, units, agent, newUnit}) => {
    const realCombatUnits = units.getCombatUnits();
    const combatUnits = [...realCombatUnits, ...units.getById(OBSERVER)]; 
    if (realCombatUnits.length >= simpleState.minUnits) {
        // simpleState.minUnits = Math.min(40, simpleState.minUnits + 2);
        return attack({combatUnits, actions, map});
    }
    
    const { foodUsed } = agent;
    if (realCombatUnits.length >= foodUsed / 7) {
        return actions.attackMove(newUnit, simpleState.danger || map.getCombatRally());
    }
    return actions.attackMove(newUnit, map.getCombatRally());
};

const yo = {    
    async onUnitFinished({ resources, agent }, newUnit) { 
        // check to see if the unit in question is a gas mine
        if (newUnit.isStructure()) {
            return onBuilduingFinished({ resources, agent }, newUnit);
        }
    },
    async onUnitCreated({ resources, agent, data }, newUnit) {
        const { units, map, actions, frame, debug } = resources.get();
        const seconds = frame.timeInSeconds();
        if (seconds <= 10) {
            return;
        }
        try {
            if (newUnit.isWorker()) {
                // const workers = units.getWorkers();
                return actions.gather(newUnit);
            }
            if (newUnit.isCombatUnit() || newUnit.unitType === OBSERVER) {
                return nonWorkerCreated({ units, actions, map, agent, newUnit});
                
            }
            // if (newUnit.isStructure()) {
            
            // }
        } catch (error) {
            console.error(error);
        }
        
    },
    async onStep({ agent, resources, data }) {
        const {minerals, vespene} = agent; 
        const { foodUsed, foodCap } = agent;
        refresh({minerals, vespene});
        const { units, map, actions, frame, debug } = resources.get();
        
        const seconds = frame.timeInSeconds();
        
        try {
            chainIfFalsyAndCatchAsync(
                () => {
                    return idleBackToWork({resources, agent, data});
                },
                () => {
                    return haveEnoughPylons({ resources, agent, data });
                },
                () => {
                    return makeProbes({data, units, seconds, actions, foodUsed, foodCap});
                },
                () => {
                    return haveTechWeWant({ resources, agent, data });
                },
                () => {
                    return makeUnits({data, units, seconds, actions, foodUsed, foodCap, agent});
                },
                
            );
            
        } catch (error) {
            console.error(error);
        }
    },
    async onUnitDamaged({ agent, resources, data }, x) {
        // todo
    },
    async onEnemyFirstSeen({ agent, resources, data }, x) {
        // todo 
    },
    async onUnitDestroyed({ agent, resources, data }, destroyedUnit) {
        try {
            // console.log(`Player #${destroyedUnit.owner} lost ${unitNames[destroyedUnit.unitType]}, we are Player #${Alliance.SELF}`);
            if (destroyedUnit.owner === Alliance.SELF) { // our unit
                if (destroyedUnit.labels.has(CONSTRUCTING)) {
                    const nextThingToBuild = destroyedUnit.labels.get(CONSTRUCTING);
                    removeFromFutureBuilduings(nextThingToBuild);
                    
                } else if (!destroyedUnit.isFinished() && destroyedUnit.isStructure()) {
                    removeFromFutureBuilduings(destroyedUnit.unitType);
                    
                }
                simpleState.danger = destroyedUnit.pos;
            } else if (destroyedUnit.owner === 16) {
                // is owner 16 the map itself (minerals dying )
                if (destroyedUnit.isMineralField()) {
                    simpleState.mineralFieldsGone += 1;
                    if (simpleState.mineralFieldsGone >= 8) {
                        buildingsWeWantOrdered.push(NEXUS, ASSIMILATOR, ASSIMILATOR)
                        simpleState.mineralFieldsGone -= 8
                    }
                }
            }
        } catch (error) {
            console.error(error);
        }
        
    },
};

//detection of ennemy army over 25 marines
// then build citalled adun ; launch storm capacity; produce high templar
export { defendColossus };

import { createAgent, taskFunctions, createSystem } from '@node-sc2/core/sc2.js';
const { build, upgrade } = taskFunctions;
import unitTypes from '@node-sc2/core/constants/unit-type.js';
const { 
    COLOSSUS,
    ASSIMILATOR,
    GATEWAY,
    NEXUS,
    CYBERNETICSCORE,
    ROBOTICSFACILITY,
    ROBOTICSBAY,
    PYLON,
} = unitTypes;


const defendColossus = createSystem({
    name: `defendColossus`,
    type: `build`,
    buildOrder: [
        [14, build(PYLON)],
        [16, build(ASSIMILATOR)],
        [17, build(GATEWAY)],
        [19, build(ASSIMILATOR)],
        [20, build(NEXUS)],
        [21, build(CYBERNETICSCORE)],
        [22, build(PYLON)],
        [25, build(ASSIMILATOR)],
        [26, build(ROBOTICSFACILITY)],
        [28, build(ASSIMILATOR)],
        [28, build(ROBOTICSBAY)],
        [30, build(PYLON)],
        // [34, upgrade(CHARGE)],
        // [34, build(COLOSSUS)],
        [38, build(PYLON)],
        // [40, build(COLOSSUS)],
        [46, build(PYLON)],
        [52, build(PYLON)],
        [58, build(PYLON)],
        [66, build(PYLON)],
    ],
});
export {greedyAF, greedy, safe}


import unitTypes from '@node-sc2/core/constants/unit-type.js';
const { 
    IMMORTAL,
    COLOSSUS,
    OBSERVER,
    ASSIMILATOR,
    GATEWAY,
    ZEALOT,
    STALKER,
    NEXUS,
    CYBERNETICSCORE,
    ROBOTICSFACILITY,
    ROBOTICSBAY,
    PYLON,
    PROBE,
} = unitTypes;

const greedyAF = [
    NEXUS, // first nexus
    NEXUS, // b2
    GATEWAY,
    ASSIMILATOR, // g1
    CYBERNETICSCORE,
    ASSIMILATOR, //g2
    ROBOTICSFACILITY,
    ASSIMILATOR,
    NEXUS, // b3
    ROBOTICSBAY,
    GATEWAY,
    GATEWAY,
    GATEWAY,
    ROBOTICSFACILITY,
    GATEWAY,
    GATEWAY,
    ASSIMILATOR,
    ASSIMILATOR,
    ASSIMILATOR,
    NEXUS, 
    GATEWAY,
    ROBOTICSFACILITY,
    ASSIMILATOR, 
    ASSIMILATOR,
];

const greedy = [
    NEXUS, // first nexus
    GATEWAY,
    ASSIMILATOR, // g1
    CYBERNETICSCORE,
    ASSIMILATOR, //g2
    NEXUS, // b2
    ROBOTICSFACILITY,
    ROBOTICSBAY,
    GATEWAY,
    GATEWAY,
    GATEWAY,
    ASSIMILATOR, //g3
    ASSIMILATOR, //g4
    NEXUS, // b3
    ROBOTICSFACILITY,
    GATEWAY,
    GATEWAY,
    ASSIMILATOR,//g5
];


const safe = [
    NEXUS, // first nexus
    GATEWAY,
    ASSIMILATOR, // g1
    CYBERNETICSCORE,
    ASSIMILATOR, //g2
    GATEWAY,
    NEXUS, // b2
    ROBOTICSFACILITY,
    ROBOTICSBAY,
    GATEWAY,
    GATEWAY,
    ASSIMILATOR, //g3
    ROBOTICSFACILITY,
    GATEWAY,
    GATEWAY,
    ASSIMILATOR,//g5
];
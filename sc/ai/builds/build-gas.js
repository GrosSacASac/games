export { buildgas };

import { taskFunctions, createSystem } from '@node-sc2/core/sc2.js';
const { build, upgrade } = taskFunctions;
import unitTypes from '@node-sc2/core/constants/unit-type.js';
const { 
    ASSIMILATOR,
    GATEWAY,
    PYLON,
} = unitTypes;


const buildgas = createSystem({
    name: `buildgas`,
    type: `build`,
    buildOrder: [
        [13, build(ASSIMILATOR)],
        [15, build(PYLON)],
        [17, build(GATEWAY)],
    ],
});